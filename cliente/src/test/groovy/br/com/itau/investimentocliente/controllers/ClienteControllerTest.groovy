package br.com.itau.investimentocliente.controllers

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.ws.rs.core.MediaType

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.test.web.servlet.MockMvc

import br.com.itau.investimentocliente.models.Cliente
import br.com.itau.investimentocliente.repositories.ClienteRepository
import br.com.itau.investimentocliente.services.ClienteService
import spock.lang.Specification
import spock.mock.DetachedMockFactory


@WebMvcTest
class ClienteControllerTest extends Specification{
	@Autowired
	ClienteService clienteService;
	
	@Autowired
	ClienteRepository clienteRepository;
	
	@Autowired
	MockMvc mockMvc;
	
	@TestConfiguration
	static class MockConfig{
		def factory = new DetachedMockFactory()
		@Bean
		ClienteService clienteService() {
			return factory.Mock(ClienteService)
		}
		@Bean
		ClienteRepository clienteRepository() {
			return factory.Mock(ClienteRepository)
		}
	}
	
	def 'deve buscar um cliente por CPF' (){
		given: 'os dados do cliente existem na base'

		String cpf = "123.123.123-12"
		Cliente cliente = new Cliente()
		cliente.setNome("joao")
		cliente.setCpf(cpf)
		Optional clienteOptional = Optional.of(cliente)

			when: 'e feito uma busca informando o cpf'
		def resposta = mockMvc.perform(get('/123.123.123-12'))

		then: 'retorne o cliente buscado'
		1 * clienteService.buscar(_) >> clienteOptional
		clienteOptional.isPresent() == true
	}
	
	def 'deve inserir um novo cliente'(){
		given: 'os dados do cliente sao informados'
		
		String cpf = "123.123.123-12"
		Cliente cliente = new Cliente()
		cliente.setNome("joao")
		cliente.setCpf(cpf)
		
		when: 'um novo cadastro e realizado'
		def resposta = mockMvc.perform(post('/')
			.contentType(MediaType.APPLICATION_JSON)
			.content('{"nome": "joao", "cpf": "123.123.123-12"}'))
		
		then: 'insira o cliente corretamente'
		1 * clienteService.cadastrar(_) >> cliente
		resposta.andExpect(status().isCreated()).andExpect(jsonPath('$.nome').value('joao'))
	}
	
}
